#include "Haste.h"

Haste::Haste(string name, Unit* unit)
	: Skill(unit)
{
	name = "Haste";
	getAgi();
}

int Haste::getAgi()
{
	return mAgi;
}

int Haste::effect()
{
	return mAgi += 2;
}

void Haste::effectWord()
{
	cout << "Agility increased by 2!" << endl;
}
