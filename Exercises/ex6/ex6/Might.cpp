#include "Might.h"

Might::Might(string name, Unit* unit)
	: Skill(unit)
{
	name = "Might";
	getPow();
}

int Might::getPow()
{
	return mPow;
}

int Might::effect()
{
	return mPow += 2;
}

void Might::effectWord()
{
	cout << "Power increased by 2!" << endl;
}
