#pragma once
#include "Skill.h"

class Might :
    public Skill
{
public:

    Might(string name, Unit* unit);

    int getPow();
    int effect() override;
    void effectWord() override;

private:

    int mPow;
};

