#pragma once
#include <string>
#include <iostream>

using namespace std;

class Unit;

class Skill
{
public:

	Skill(Unit* unit);
	
	string getName();
	int getHp();
	int getPow();
	int getVit();
	int getDex();

	void displayStats(Unit* unit);

	virtual int effect() = 0;
	virtual void effectWord() = 0;

private:

	string mName;
	int mHp;
	int mPow;
	int mVit;
	int mDex;
};

