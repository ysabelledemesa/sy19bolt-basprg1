#pragma once
#include "Skill.h"

class Haste :
    public Skill
{
public:

    Haste(string name, Unit* unit);

    int getAgi();
    int effect() override;
    void effectWord() override;

private:

    int mAgi;
};

