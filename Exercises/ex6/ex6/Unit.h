#pragma once
#include <string>
#include <iostream>
#include "Skill.h"

using namespace std;

class Unit
{
public:

	Unit(string name, int hp, int pow, int vit, int dex, int agi);

	string getName();
	int getHp();
	int getPow();
	int getVit();
	int getDex();
	int getAgi();

	void skill();
	void displayStats(Unit* unit);

private:

	string mName;
	int mHp;
	int mPow;
	int mVit;
	int mDex;
	int mAgi;
};

