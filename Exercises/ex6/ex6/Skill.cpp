#include "Skill.h"

Skill::Skill(Unit* unit)
{
}

string Skill::getName()
{
	return mName;
}

int Skill::getHp()
{
	return mHp;
}

int Skill::getPow()
{
	return mPow;
}

int Skill::getVit()
{
	return mVit;
}

int Skill::getDex()
{
	return mDex;
}

void Skill::displayStats(Unit* unit)
{
	cout << getHp() << endl;
	cout << getPow() << endl;
	cout << getVit() << endl;
	cout << getDex() << endl;
}