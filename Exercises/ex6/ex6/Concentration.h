#pragma once
#include "Skill.h"

class Concentration :
    public Skill
{
public:

    Concentration(string name, Unit* unit);

    int getDex();
    int effect() override;
    void effectWord() override;

private:

    int mDex;
};

