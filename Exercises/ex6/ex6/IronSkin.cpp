#include "IronSkin.h"

IronSkin::IronSkin(string name, Unit* unit)
	: Skill(unit)
{
	name = "Iron Skin";
	getVit();
}

int IronSkin::getVit()
{
	return mVit;
}

int IronSkin::effect()
{
	return mVit += 2;
}

void IronSkin::effectWord()
{
	cout << "Vitality increased by 2!" << endl;
}
