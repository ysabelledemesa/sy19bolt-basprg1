#pragma once
#include "Skill.h"

class IronSkin :
    public Skill
{
public:

    IronSkin(string name, Unit* unit);

    int getVit();
    int effect() override;
    void effectWord() override;

private:

    int mVit;
};

