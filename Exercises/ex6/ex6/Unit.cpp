#include "Unit.h"

Unit::Unit(string name, int hp, int pow, int vit, int dex, int agi)
{
    mName = name;
    mHp = hp;
    mPow = pow;
    mVit = vit;
    mDex = dex;
    mAgi = agi;
}

string Unit::getName()
{
    return mName;
}

int Unit::getHp()
{
    return mHp;
}

int Unit::getPow()
{
    return mPow;
}

int Unit::getVit()
{
    return mVit;
}

int Unit::getDex()
{
    return mDex;
}

int Unit::getAgi()
{
    return mAgi;
}

void Unit::skill()
{
}

void Unit::displayStats(Unit* unit)
{
    cout << getHp() << endl;
    cout << getPow() << endl;
    cout << getVit() << endl;
    cout << getDex() << endl;
}
