#include <iostream>
#include <string>
#include <vector>
#include "Unit.h"
#include "Skill.h"
#include "Heal.h"
#include "Might.h"
#include "IronSkin.h"
#include "Concentration.h"
#include "Haste.h"

using namespace std;

int main()
{
	Unit* unit = new Unit("Character", 0, 0, 0, 0, 0);
	vector<Skill*> skills;

	Heal* heal = new Heal("Heal", unit);
	Might* might = new Might("Might", unit);
	IronSkin* ironSkin = new IronSkin("Iron Skin", unit);
	Concentration* conc = new Concentration("Concentration", unit);
	Haste* haste = new Haste("Haste", unit);

	skills.push_back(heal);
	skills.push_back(might);
	skills.push_back(ironSkin);
	skills.push_back(conc);
	skills.push_back(haste);

	while (true)
	{
		int i = rand() % skills.size();
		Skill* skill = skills[i];
		cout << skills[i]->getName() << " was used!" << skills[i]->effectWord() << endl;
		cout << skill->displayStats(unit) << endl;
	}

	return 0;
}