#include "Concentration.h"

Concentration::Concentration(string name, Unit* unit)
	:Skill(unit)
{
	name = "Concentration";
	getDex();
}

int Concentration::getDex()
{
	return mDex;
}

int Concentration::effect()
{
	return mDex += 2;
}

void Concentration::effectWord()
{
	cout << "Dexterity increased by 2!" << endl;
}
