#include "Heal.h"

Heal::Heal(string name, Unit* unit)
	: Skill(unit)
{
	name = "Heal";
	getHp();
}

int Heal::getHp()
{
	return mHp;
}

int Heal::effect()
{
	return mHp += 10;
}

void Heal::effectWord()
{
	cout << "Health increased by 10!" << endl;
}
