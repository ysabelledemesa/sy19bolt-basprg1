#pragma once
#include "Skill.h"

class Heal :
    public Skill
{
public:

    Heal(string name, Unit* unit);

    int getHp();
    int effect() override;
    void effectWord() override;

private:

    int mHp;
};

