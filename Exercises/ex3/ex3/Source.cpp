#include <iostream>
#include <time.h>
#include <string>

using namespace std;


// exercise 3.1

void fill(int* numbers)
{
	for (int i = 0; i < 10; i++)
	{
		numbers[i] = rand() % 200;
	}
}

void print(int* numbers)
{
	for (int i = 0; i < 10; i++)
	{
		cout << numbers[i] << endl;
	}
}

void remove(int* numbers)
{
	for (int i = 0; i < 10; i++)
	{
		delete[] numbers;
	}
}

int main()
{
	srand(time(0));

	int array1[10];

	int* arrayPtr = array1;
	fill(arrayPtr);
	print(arrayPtr);

	cout << endl << endl;

	int* array2 = new int;
	fill(array2);
	print(array2);
	remove(array2);
}