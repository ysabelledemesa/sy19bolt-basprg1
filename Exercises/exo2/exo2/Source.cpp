#include <iostream>
#include <time.h>
#include <string>
#include <vector>

using namespace std;

void printVector(const vector<string>& items)
{
	for (int i = 0; i < items.size(); i++)
	{
		cout << items[i] << endl;
	}
}

void populateVector(vector<string>& items)
{
	for (int i = 0; i < 10; i++)
	{
		string words[] = { "RedPotion", "Elixir", "EmptyBottle", "BluePotion" };
		items.push_back(words[rand() % 4]);
	}
}

void count(vector<string>& items)
{
	for (int i = 0; i < items.size(); i++)
	{
		if (i == i)
		{
			cout << "There has been a repeat." << endl;
		}
	}
}

int main()
{
	srand(time(0));

	vector<string> items;

	populateVector(items);

	printVector(items);

	items.erase(items.begin() + 2);

}