#include <iostream>
#include <time.h>
#include <string>

using namespace std;

void bet(int actorBet, int& actorGold)
{
	actorGold -= actorBet;
}

void roll(int& dice1, int& dice2)
{
	srand(time(NULL));

	dice1 = rand() % 5 + 1;
	dice2 = rand() % 5 + 1;
}

void win(int dicesum1, int dicesum2, int& gold, int& actorBet)
{
	if (dicesum1 < dicesum2) gold -= actorBet;
	if (dicesum1 > dicesum2) gold += actorBet;
	if (dicesum1 = dicesum2) gold;

}

void playRound(int& playerGold)
{
	if (playerGold <= 0) return;
}

int main()
{
	int playerGold = 1000;
	int playerBet;

	cout << "How much do you want to bet?" << endl;
	cin >> playerBet;

	bet(playerBet, playerGold);

	cout << "You have " << playerGold << " gold left." << endl;

	int playerDice1, playerDice2;
	int AIDice1, AIDice2;

	roll(playerDice1, playerDice2);
	roll(AIDice1, AIDice2);

	cout << "Player's roll is " << playerDice1 << " and " << playerDice2 << endl;

	cout << "AI's roll is " << AIDice1 << " and " << AIDice2 << endl;

	int playerSum = playerDice1 + playerDice2;
	int AISum = AIDice1 + AIDice2;

	win(playerSum, AISum, playerGold, playerBet);

	cout << "You have " << playerGold << " left." << endl;

	playRound(playerGold);
}