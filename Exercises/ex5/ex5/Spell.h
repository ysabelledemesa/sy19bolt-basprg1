#pragma once
#include <string>
#include <iostream>

using namespace std;

class Wizard;

class Spell
{
public:

	void activate(Wizard* target);

	string getName();
	int getMinDamage();
	int getMaxDamage();
	int getMPCost();

	void setName(string name);
	void setMinDamage(int value);
	void setMaxDamage(int value);
	void setMPCost(int value);

private:

	string mName;
	int mMinDamage;
	int mMaxDamage;
	int mMPCost;
};

