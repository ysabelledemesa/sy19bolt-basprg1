#include "Wizard.h"
#include <time.h>

void Wizard::attack(Wizard* target)
{
	cout << mName << " used attack!" << endl;
	int damage = rand() % 5 + 10;
	target->setHp(target->getHp() - damage);
	cout << target->mName << " has only " << target->mHp << " HP left!" << endl;

	mMp += rand() % 10 + 10;
	cout << mName << " earned " << mMp << " MP!" << endl;
}

string Wizard::getName()
{
	return mName;
}

int Wizard::getHp()
{
	return mHp;
}

int Wizard::getMp()
{
	return mMp;
}

int Wizard::getMinDamage()
{
	return mMinDamage;
}

int Wizard::getMaxDamage()
{
	return mMaxDamage;
}

void Wizard::setName(string name)
{
	mName = name;
}

void Wizard::setHp(int value)
{
	mHp = value;
	if (mHp < 0) mHp = 0;
}

void Wizard::setMp(int value)
{
	mMp = value;
}

void Wizard::setMinDamage(int value)
{
	mMinDamage = value;
}

void Wizard::setMaxDamage(int value)
{
	mMaxDamage = value;
}
