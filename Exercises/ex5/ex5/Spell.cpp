#include "Spell.h"
#include "Wizard.h"

void Spell::activate(Wizard* target)
{
	if (target->getMp() >= 50)
	
	cout << "spell used!" << endl;

	int damage = rand() % 20 + 40;
	target->setHp(target->getHp()- damage);

	cout << mName << " used a spell on " << target->getName() << "." << endl;

	cout << target->getName() << " only has " << target->getHp() << " HP left!" << endl;

	cout << target->getName() << " only has " << target->getMp() << " MP left!" << endl;
}

string Spell::getName()
{
	return mName;
}

int Spell::getMinDamage()
{
	return mMinDamage;
}

int Spell::getMaxDamage()
{
	return mMaxDamage;
}

int Spell::getMPCost()
{
	return mMPCost;
}

void Spell::setName(string name)
{
	mName = name;
}

void Spell::setMinDamage(int value)
{
	mMinDamage = value;
}

void Spell::setMaxDamage(int value)
{
	mMaxDamage = value;
}

void Spell::setMPCost(int value)
{
	mMPCost = value;
}

int getHp(Wizard*)
{
	return getHp();
}
