#pragma once
#include <string>
#include <iostream>

using namespace std;

class Spell;

class Wizard
{
public:

	void attack(Wizard* target);

	string getName();
	int getHp();
	int getMp();
	int getMinDamage();
	int getMaxDamage();

	void setName(string name);
	void setHp(int value);
	void setMp(int value);
	void setMinDamage(int value);
	void setMaxDamage(int value);

private:
	string mName;
	int mHp;
	int mMp;
	int mMinDamage;
	int mMaxDamage;
};
