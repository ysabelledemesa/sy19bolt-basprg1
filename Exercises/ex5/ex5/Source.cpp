#include <iostream>
#include <string>
#include <time.h>
#include "Wizard.h"
#include "Spell.h"

using namespace std;

int main()
{
	Wizard* wizard1 = new Wizard();
	wizard1->setName("Houdini");
	wizard1->setHp(250);
	wizard1->setMp(0);
	wizard1->setMinDamage(10);
	wizard1->setMaxDamage(15);

	Wizard* wizard2 = new Wizard();
	wizard2->setName("Merlin");
	wizard2->setHp(250);
	wizard2->setMp(0);
	wizard2->setMinDamage(10);
	wizard2->setMaxDamage(15);

	wizard1->attack(wizard2);
	wizard2->attack(wizard1);
	
	Spell* spell = new Spell();
	wizard1->activate(wizard2);
	wizard2->activate(wizard1);

}