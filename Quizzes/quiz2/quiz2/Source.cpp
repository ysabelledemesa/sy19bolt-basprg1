#ifndef NODE_H
#define NODE_H

#include <string>
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

struct Node
{
	std::string name; //actual object or character for the output
	Node* next = NULL; //holds location of the next node
	Node* previous = NULL; //
};

#endif //NODE_H

Node* circularList(); //Creates a circular list
void printMembers(Node* head); //Prints all members of the Night's Watch at the beginning
Node* randomStart(Node* head); //Randomly chooses a member to hold the cloak
Node* playRound(Node* member); //Actual "game"
Node* returnFunction(Node* head); //Returns final node at the end of a for loop
void printPlaying(Node* head, int size); //Prints the remaining members as more step out of the circle
void selectedMember(Node* head); //Selects and prints the chosen member to seek reinforcements in the end

int main()
{
	srand(time(NULL));

	Node* newNode = circularList();

	printMembers(newNode);

	//assigning the result from randomStart to random
	Node* random = randomStart(newNode);

	//using random as the parameter for playRound
	//assigning the result of playRound to final
	Node* final = playRound(random);

	//using final as the parameter for playRound
	selectedMember(final);

	return 0;
}

Node* circularList()
{
	//helps build nodes within linked list
	Node* head = nullptr;
	Node* current = nullptr;
	Node* previous = nullptr;

	//First Node
	current = new Node; //will be reusing the "current" pointer all throughout the game
	current->name = "Aliser";
	head = current;
	previous = current;

	//Second Node
	current = new Node;
	current->name = "Janos";
	previous->next = current; //this links the first node to this node
	previous = current;

	//Third Node
	current = new Node;
	current->name = "Othell";
	previous->next = current;
	previous = current;

	//Fourth Node
	current = new Node;
	current->name = "Sam";
	previous->next = current;
	previous = current;

	//Fifth Node
	current = new Node;
	current->name = "Snow";
	previous->next = current;
	current->next = head; //circular linked list. the tail shouldn't lead to NULL but back to the head
	previous = current;

	return head;
}

//Prints all members of the Night's Watch at the beginning
void printMembers(Node * head)
{
	Node* currentHead = head; //using a pointer for the parameter to avoid touching the real value in main

	while (currentHead != NULL)
	{
		for (int i = 0; i < 5; i++) //for loop since repeating for each member. transversing through list
		{
			cout << "What is your name?" << endl;
			cout << "I am " << currentHead->name << ", Sir." << endl;
			currentHead = currentHead->next; //this turns the currentHead to the next node
		}

		cout << endl << endl;

		return;
	}
}

//Randomly chooses a member to hold the cloak
Node* randomStart(Node* head)
{	
	Node* current = head; //another pointer for the parameter

	while (current != NULL)
	{
		srand(time(NULL));

		int random = rand() % 4 + 1; //randomly choose between all 5 members

		current = head;

		for (int i = 0; i < random; i++) //for loop that transverses through list by the randomized number
		{
			current = current->next; //move to the next node
		}
		//result of the for loop will be displayed here
		cout << current->name << " has been chosen by the Lord Commander to be the first to hold the cloak of the Night's Watch." << endl;
		cout << endl << endl;

		return current; //will be used later for the start of the playRound function
	}
}

//Actual "game"
Node* playRound(Node* member)
{
	int size = 4; //number of members minus one (will add 1 later)

	Node* newHead = member; //pointer for actual parameter

	for (int i = 0; i < 4; i++)
	{
		Node* currentHead = newHead; //currentHead is the current cloak holder while newHead is the holder after someone is eliminated
		Node* toRemove = member; // toRemove is the node to be removed from the list or the member to be eliminated

		cout << "Round " << i + 1 << endl; //added 1 to avoid Round 0;
		cout << "The remaining members are:" << endl;
		printPlaying(currentHead, size + 1); //displays current members still within the circle
		cout << endl << endl;

		int draw = (rand() % size) + 1; //subtracted 1 in int size and added 1 now for the purpose of avoiding a random number of 0

		cout << currentHead->name << " has drawn " << draw << "." << endl;

		for (int i = 0; i < draw; i++) //transverses through list based on the random number or draw
		{
			currentHead = toRemove; //keep track of previous node
			cout << "..." << toRemove->name << "..." << endl;
			toRemove = toRemove->next; //move to the next node
		}

		currentHead->next = toRemove->next; //reconnectinng the gaps of the two nodes

		cout << "The cloak is passed to " << toRemove->name << "." << endl;
		cout << toRemove->name << " is eliminated." << endl;
		
		newHead = toRemove->next; //assigning the new cloak holder (the member next to the eliminated one)
		
		cout << toRemove->name << " hands the cloak to " << newHead->name << " as he steps back." << endl;
		cout << endl << endl;

		delete toRemove; //deleting the node;
		toRemove = nullptr;

		size = size - 1; //reducing the size of the group by 1 every time the loop repeats

		returnFunction(newHead); //returning the value wihtout ending the loop
	}
	return newHead;
}

//Returns final node at the end of a for loop
Node* returnFunction(Node* head)
{
	return head;
}

//Prints the remaining members as more step out of the circle
void printPlaying(Node* head, int size)
{
	Node* currentHead = head; //pointer to the actual parameter

		for (int i = 0; i < size; i++) //transversing the list based on the number of people
		{
			cout << currentHead->name << endl;
			currentHead = currentHead->next; //move to the next node
		}
}

//Selects and prints the chosen member to seek reinforcements in the end
void selectedMember(Node* head)
{
	cout << head->name << " is the final one within the circle. He shall hold the cloak and be the one to seek reinforcements." << endl;
}



