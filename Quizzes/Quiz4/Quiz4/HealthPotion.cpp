#include "HealthPotion.h"

HealthPotion::HealthPotion(string name, int heal)
	: Item(name)
{
	mHeal = heal;
}

HealthPotion::~HealthPotion()
{
}

void HealthPotion::pulled(Player* player) //adds to player's HP upon pulling a Health Potion
{
	Item::pulled(player);

	player->heal(mHeal);

	cout << player->getName() << " has been healed by " << mHeal << " HP" << endl;
}
