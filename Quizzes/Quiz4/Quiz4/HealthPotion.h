#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class HealthPotion :
    public Item
{
public:
    HealthPotion(string name, int heal);
    ~HealthPotion();

    void pulled(Player* player) override; //overriding parent function in Item.h

private:
    int mHeal;
};

