#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <cstdlib>
#include "Player.h"

using namespace std;

class Player;

class Item
{
public:
	Item(string name);
	~Item();

	//getters
	string getName();
	Player* getPlayer();

	//virtual function to be overridden by derived classes later
	//demonstrates pulling of an item
	virtual void pulled(Player* player) = 0; 

private:
	string mName;
	Player* mPlayer;
	int count;
};

