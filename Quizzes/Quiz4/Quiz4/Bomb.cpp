#include "Bomb.h"

Bomb::Bomb(string name, int damage)
	: Item(name)
{
	mDamage = damage;
}

Bomb::~Bomb()
{
}

void Bomb::pulled(Player* player) //deduct player's HP after pulling Bomb
{
	Item::pulled(player);

	player->damage(mDamage);

	cout << player->getName() << " loses " << mDamage << " HP!" << endl;
}