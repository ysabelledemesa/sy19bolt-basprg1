#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class Bomb :
    public Item
{
public:
    Bomb(string name, int damage);
    ~Bomb();

    void pulled(Player* player) override; //overriding parent function in Item.h

private:
    int mDamage;
};

