#include "Crystal.h"

Crystal::Crystal(string name, int crystal)
	:Item(name)
{
	mCrystal = crystal;
}

Crystal::~Crystal()
{
}

void Crystal::pulled(Player* player) //adds to player's crystals upon pulling Crystal
{
	Item::pulled(player);

	player->addCrystals(mCrystal);

	cout << player->getName() << " has received " << mCrystal << " crystals!" << endl;
}
