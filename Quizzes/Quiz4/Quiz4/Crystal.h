#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class Crystal :
    public Item
{
public:
    Crystal(string name, int crystal);
    ~Crystal();

    void pulled(Player* player) override; //overriding parent function in Item.h

private:
    int mCrystal;
};

