#include "Player.h"

Player::Player(string name)
{
	mName = name;
}

Player::~Player()
{
}

string Player::getName() //returns player's name
{
	return mName;
}

int Player::getHp() //returns player's HP
{
	return mHp;
}

int Player::getCrystal() //returns player's Crystals
{
	return mCrystal;
}

int Player::getRarity() //returns player's Rarity Points
{
	return mRarity;
}

vector<Item*>& Player::getItems() //returns items
{
	return mItems;
}

void Player::addItem(Item* item) //pushes back initialized items into vector
{
	mItems.push_back(item);
}

bool Player::alive() //ensures that player still has an HP above 0
{
	return mHp > 0;
}

void Player::heal(int heal) //adds to player's health
{
	if (heal < 1) throw exception("Heal value too low!");
	mHp += heal;
}

void Player::addCrystals(int crystal) //adds to player's crystals
{
	mCrystal += crystal;
}

void Player::addRarity(int rarityPoints) //adds to player's rarity points
{
	mRarity += rarityPoints;
}

void Player::damage(int damage) //deducts player's health
{
	mHp -= damage;
}

void Player::pullCost() //deducts player's crystals 
{
	cout << "By pulling, 5 crystals have been deducted." << endl;
	mCrystal -= 5;
}

void Player::displayStats() //prints player's stats
{
	cout << "Name: " << mName << endl;
	cout << "HP: " << mHp << endl;
	cout << "Crystals: " << mCrystal << endl;
	cout << "Rarity Points: " << mRarity << endl;
}
