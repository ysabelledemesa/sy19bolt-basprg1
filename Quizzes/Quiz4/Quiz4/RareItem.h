#pragma once
#include "Item.h"
#include <string>
#include <iostream>

using namespace std;

class RareItem :
    public Item
{
public:
    RareItem(string name, int rarityPoints);
    ~RareItem();

    void pulled(Player* player) override; //overriding parent function in Item.h

private:
    int mRarityPoints;
};

