#include "Item.h"

Item::Item(string name)
{
	mName = name;
}

Item::~Item()
{
}

string Item::getName() //returns item name
{
	return mName;
}

Player* Item::getPlayer() //returns player
{
	return mPlayer;
}

void Item::pulled(Player* player) //pulling item
{
	if (player == NULL)
	{
		throw new exception("Player is NULL.");
	}
}

