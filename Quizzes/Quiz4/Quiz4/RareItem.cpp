#include "RareItem.h"

RareItem::RareItem(string name, int rarityPoints)
	: Item(name)
{
	mRarityPoints = rarityPoints;
}

RareItem::~RareItem()
{
}

void RareItem::pulled(Player* player) //adds rarity points to player upon pulling a rare item
{
	Item::pulled(player);

	player->addRarity(mRarityPoints);

	cout << player->getName() << " has received " << mRarityPoints << " rarity points!" << endl;
}

