#pragma once
#include <string>
#include <cstdlib>
#include <iostream>
#include <vector>
#include "Item.h"

using namespace std;

class Item;

class Player
{
public:
	Player(string name);
	~Player();

	//getters
	string getName();
	int getHp();
	int getCrystal();
	int getRarity();

	vector<Item*>& getItems(); //vector for items, to be linked with the player later on

	void addItem(Item* item); //pushing back initialized items to the vector "getItems"

	bool alive(); //ensures that the player still has an HP above 0
	void heal(int heal); //adds to player HP if Health Potion is pulled
	void addCrystals(int crystal); //adds to player Crystals if Crystal is pulled
	void addRarity(int rarityPoints); //adds to player Rarity Points if a rare item is pulled
	void damage(int damage); //deducts player HP is Bomb is pulled
	void pullCost(); //deducts player Crystals each turn (currency)
	void displayStats(); //prints player stats

private:
	string mName = "Player";
	int mHp = 100;
	int mCrystal = 100;
	int mRarity = 0;
	vector<Item*> mItems;
};

