#include <string>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include "Player.h"
#include "RareItem.h"
#include "HealthPotion.h"
#include "Bomb.h"
#include "Crystal.h"
#include <vector>

using namespace std;

Player* createPlayer(); //creates a new player and contains the items for the gacha pull
void probabilty(vector<Item*> item); //sets up the probability of picking an item based on the given table

int main()
{
	srand(time(NULL)); //for randomly choosing an item

	Player* player = createPlayer(); //"player" is set as the result of the "createPlayer" function
	vector<Item*> item = player->getItems(); //vector that contains all of the items to be pulled
	vector<Item*> pulledItems; //vector to keep track of all pulled items, to be used later

	//conditions for the game to continue 
	//game ends when player dies/lacks sufficient amount of crystals/reached 100 rarity
	while (player->getRarity() < 100 || player->getHp() > 0 || player->getCrystal() > 0)
	{
		player->displayStats(); //player's stats(HP, Crystals, Accumulated Rarity) are always displayed

		int itemIndex = rand() % player->getItems().size(); //random number based on the vector size of items
		probabilty(player->getItems()); //sets up probability within the items
		Item* item = player->getItems()[itemIndex]; //a new item, with the random number as the index of the vector

		system("pause"); //mostly for a neat output
		cout << endl; //neat output

		player->pullCost(); //deducts 5 crystals from player for each pull
		cout << endl << "Player has pulled " << item->getName() << "." << endl; //shows item pulled
		item->pulled(player); //shows item's effects on player 

		pulledItems.push_back(item); //pushes each pulled item into vector, keeping track of pulled items

		system("pause"); //neat output
		system("cls"); //neat output

		//conditions where game ends:
		if (player->getRarity() >= 100) //player has rarity points >= 100
		{
			cout << "The player has won!" << endl << endl;
			break;
		}

		if (player->getHp() <= 0) //player has died/ HP below or equal to 0
		{
			cout << "The player has died..." << endl << endl;
			break;
		}

		if (player->getCrystal() <= 0) //player ran out of crystals in order to pull an item
		{
			cout << "The player ran out of crystals and can no longer proceed..." << endl << endl;
			break;
		}
	}

	player->displayStats(); //displays player's stats after game ends
	cout << endl;

	//GAME SUMMARY
	//PRINT OUT ALL PULLED ITEMS IN THE ORDER THEY WERE PULLED

	//for (int i = 0; i < pulledItems.size(); i++)
	//{
	//	cout << pulledItems[i]->getName() << endl;
	//}

	//collating all pulled items
	int repeat0 = 0; //for index 0
	int repeat1 = 0; //for index 1
	int repeat2 = 0; //for index 2
	int repeat3 = 0; //for index 3
	int repeat4 = 0; //for index 4
	int repeat5 = 0; //for index 5

	for (int i = 0; i < pulledItems.size(); i++)
	{
		string itemName = pulledItems[i]->getName(); //creating a variable for every item name for convenience

		if (itemName == "SSR")
		{
			repeat0++; //for every pulled item that has the name "SSR", count goes up by one
		}

		else if (itemName == "SR")
		{
			repeat1++; //for every pulled item that has the name "SR", count goes up by one
		}

		else if (itemName == "R")
		{
			repeat2++; //for every pulled item that has the name "R", count goes up by one
		}

		else if (itemName == "Health Potion")
		{
			repeat3++; //for every pulled item that has the name "Health Potion", count goes up by one
		}

		else if (itemName == "Bomb")
		{
			repeat4++; //for every pulled item that has the name "Bomb", count goes up by one
		}

		else if (itemName == "Crystal")
		{
			repeat5++; //for every pulled item that has the name "Crystal", count goes up by one
		}
	}

	//printing all collated items
	cout << item[0]->getName() << " x" << repeat0 << endl;
	cout << item[1]->getName() << " x" << repeat1 << endl;
	cout << item[2]->getName() << " x" << repeat2 << endl;
	cout << item[3]->getName() << " x" << repeat3 << endl;
	cout << item[4]->getName() << " x" << repeat4 << endl;
	cout << item[5]->getName() << " x" << repeat5 << endl;

	delete player; //deallocating dynamic memory

	return 0;
}

//creates a new player and contains the items for the gacha pull
Player* createPlayer()
{
	Player* player = new Player("Player"); //allocating and initializing memory for player

	Item* ssr = new RareItem("SSR", 50); //allocating and initializing memory for SSR
	player->addItem(ssr); //pushing memory into the items vector 

	Item* sr = new RareItem("SR", 10); //allocating and initializing memory for SR
	player->addItem(sr); //pushing memory into the items vector 
	
	Item* r = new RareItem("R", 1); //allocating and initializing memory for R
	player->addItem(r); //pushing memory into the items vector 

	Item* healthPotion = new HealthPotion("Health Potion", 30); //allocating & initializing for Health Potion
	player->addItem(healthPotion); //pushing memory into the items vector 

	Item* bomb = new Bomb("Bomb", 25); //allocating and initializing memory for Bomb
	player->addItem(bomb); //pushing memory into the items vector 

	Item* crystal = new Crystal("Crystal", 15); //allocating and initializing memory for Crystal
	player->addItem(crystal); //pushing memory into the items vector 

	return player;
}

//sets up the probability of picking an item based on the given table
void probabilty(vector<Item*> item)
{
	srand(time(NULL)); //for random

	double prob = (double)rand() / RAND_MAX; //random value from the maximum value

	if (prob < 0.01) item[0]; //SSR has a pulling chance of 1%
	else if (prob < 0.09) item[1]; //SR has a pulling chance of 9%
	else if (prob < 0.4) item[2]; //R has a pulling chance of 40%
	else if (prob < 0.15) item[3]; //Health Potion has a pulling chance of 15%
	else if (prob < 0.2) item[4]; //Bomb has a pulling chance of 20%
	else if (prob < 0.15) item[5]; //Crystal has a pulling chance of 15%
	else NULL;
}
