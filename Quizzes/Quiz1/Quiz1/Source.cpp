#include <iostream>
#include <time.h>
#include <string>
#include <vector>

using namespace std;

void printCards(const vector<string>& menu)
{
	for (int i = 0; menu.size(); i++)
	{
		cout << menu[i] << endl;
	}
}

void populateCards(vector<string>& menu)
{
	for (int i = 0; i < 5; i++)
	{
		string menu[] = { "[1] Emperor", "[2] Citizen", "[3] Citizen", "[4] Citizen", "[5} Citizen" };
	}
}

void emperorsidewin(int& playermmbet, int& playermoneyearned)
{
	playermoneyearned += playermmbet * 100000;
}

void emperorsidelose(int& playermmbet, int& playermmleft)
{
	playermmleft -= playermmbet;
}

void EmperorplayRound(int playerRound, int& playermoneyearned, int& playermmleft)
{
	cout << "Cash: " << playermoneyearned << endl;
	cout << "Distance left (mm): " << playermmleft << endl;
	cout << "Round: " << playerRound << endl;
	cout << "Side: Emperor" << endl;

	cout << endl << endl;

	cout << "How many mm would you like to wager, Kaiji?" << endl;
	int mmBet;
	cin >> mmBet;

	cout << endl << endl;

	cout << "Pick a card to play." << endl;
	cout << endl << endl;
	cout << "[1] Emperor" << endl;
	cout << "[2] Citizen" << endl;
	cout << "[3] Citizen" << endl;
	cout << "[4] Citizen" << endl;
	cout << "[5] Citizen" << endl;

	int playerCard;
	cout << endl << endl;
	cout << "Card of Choice: " << endl;
	cin >> playerCard;

	int opponentCard = rand() % 4 + 1;

	if (playerCard == 1 && opponentCard == 1)
	{
		cout << "The opponent's slave has beaten your emperor." << endl;
		cout << "The drill goes closer to your eardrum." << endl;
		emperorsidelose(mmBet, playermmleft);
	}

	if (playerCard > 1 && opponentCard == 1)
	{
		cout << "Your citizen has beat the opponent's slave." << endl;
		emperorsidewin(mmBet, playermoneyearned);
	}

	if (playerCard == 1 && opponentCard > 1)
	{
		cout << "Your emperor has beat the opponent's citizen." << endl;
		emperorsidewin(mmBet, playermoneyearned);
	}

	if (playerCard > 1 && opponentCard > 1)
	{
		cout << "You both drew Citizens. It's a draw!" << endl;
	}
}

void slavesidewin(int& playermmbet, int& playermoneyearned)
{
	playermoneyearned += playermmbet * 500000;
}

void slavesidelose(int& playermmbet, int& playermmleft)
{
	playermmleft -= playermmbet;
}

void SlaveplayRound(int playerRound, int& playermoneyearned, int& playermmleft)
{
	cout << "Cash: " << playermoneyearned << endl;
	cout << "Distance left (mm): " << playermmleft << endl;
	cout << "Round: " << playerRound << endl;
	cout << "Side: Slave" << endl;

	cout << endl << endl;

	cout << "How many mm would you like to wager, Kaiji?" << endl;
	int mmBet;
	cin >> mmBet;

	cout << endl << endl;

	cout << "Pick a card to play." << endl;
	cout << endl << endl;
	cout << "[1] Slave" << endl;
	cout << "[2] Citizen" << endl;
	cout << "[3] Citizen" << endl;
	cout << "[4] Citizen" << endl;
	cout << "[5] Citizen" << endl;

	int playerCard;
	cout << endl << endl;
	cout << "Card of Choice: " << endl;
	cin >> playerCard;

	int opponentCard = rand() % 4 + 1;

	if (playerCard == 1 && opponentCard == 1)
	{
		cout << "Your slave has beaten the opponent's emperor." << endl;
		slavesidewin(mmBet, playermoneyearned);
	}

	if (playerCard > 1 && opponentCard == 1)
	{
		cout << "Your citizen has been beaten by the opponent's emperor." << endl;
		cout << "The drill goes closer to your eardrum." << endl;
		slavesidelose(mmBet, playermmleft);
	}

	if (playerCard == 1 && opponentCard > 1)
	{
		cout << "Your slave has been beaten by the opponent's citizen." << endl;
		cout << "The drill goes closer to your eardrum." << endl;
		slavesidelose(mmBet, playermmleft);
	}

	if (playerCard > 1 && opponentCard > 1)
	{
		cout << "You both drew Citizens. It's a draw!" << endl;
	}
}

int main()
{
	srand(time(0));
	
	int round = 1;
	int mmLeft = 30;
	int moneyEarned = 0;

	while (round <= 3 && mmLeft > 0 && moneyEarned < 20000000)
	{
		vector<string> menu;

		EmperorplayRound(round, moneyEarned, mmLeft);

		populateCards(menu);

		printCards(menu);

		round++;
	}

	while (round > 3 && round <= 6 && moneyEarned < 20000000)
	{
		vector<string> menu;

		SlaveplayRound(round, moneyEarned, mmLeft);

		populateCards(menu);

		printCards(menu);

		round++;
	}

	while (round > 6 && round <= 9 && moneyEarned < 20000000)
	{
		vector<string> menu;

		EmperorplayRound(round, moneyEarned, mmLeft);

		populateCards(menu);

		printCards(menu);

		round++;
	}

	while (round > 9 && round <= 12 && moneyEarned < 20000000)
	{
		vector<string> menu;

		SlaveplayRound(round, moneyEarned, mmLeft);

		populateCards(menu);

		printCards(menu);

		round++;
	}

	while (round >= 12 || mmLeft <= 0)
	{
		cout << "You have reached the end of this game." << endl;

		if (moneyEarned >= 20000000 && mmLeft > 0)
		{
			cout << "Congratulations. You have won 20,000,000." << endl;
		}

		if (moneyEarned < 20000000 && mmLeft > 0)
		{
			cout << "You didn't get your 20,000,000 but at least you got to keep your ear." << endl;
		}

		if (mmLeft <= 0)
		{
			cout << "Better luck next time..." << endl;
		}
	}

	cout << endl;
	return 0;
}