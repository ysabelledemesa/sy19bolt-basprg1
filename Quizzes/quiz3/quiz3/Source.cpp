#include <string>
#include <iostream>
#include "Unit.h"
#include <time.h>

using namespace std;

Unit* characterCreation(Unit* unit); //asks player to input a name and select a character class
void warriorStats(string name, Unit* unit); //contains the values and attributes of the warrior class
void mageStats(string name, Unit* unit); //contains the values and attributes of the mage class
void assassinStats(string name, Unit* unit); //contains the values and attributes of the assassin class
void displayInfo(Unit* unit); //print the statistics of each class
float classStrength(Unit* player, Unit* enemy); //determines which classes are stronger and weaker against the other
Unit* enemySpawner(string name, Unit* unit); //creates a random enemy with a random class
void playRound(Unit* playerClass, Unit* enemy, int round); //actual game
void win(Unit* player, Unit* enemy); //contains rewards for the player winning
void lose(Unit* player, Unit* enemy, int round); //shows the defeat and the game progress

int main()
{
	Unit Warrior, Mage, Assassin;
	//create a new operator for each instance of the class
	Unit* player = new Unit;
	Unit* enemy = new Unit;
	Unit* warrior = new Unit;
	Unit* mage = new Unit;
	Unit* assassin = new Unit;

	//will be used later to count how many levels have passed
	int roundNumber = 0;

	//assigning the stats inside the function to the instances created
	Unit unit; 
	warriorStats("Warrior", warrior);
	mageStats("Mage", mage);
	assassinStats("Assassin", assassin);

	//assigning playerClass as the result of the player's choices
	Unit* playerClass = characterCreation(player);

	system("pause");
	cout << endl;

	//actual game
	playRound(playerClass, enemy, roundNumber);
}

//asks player to input a name and select a character class
Unit* characterCreation(Unit* unit)
{
	//asks player to input name
	cout << "What is your name?" << endl;
	string name;
	cin >> name;

	//pointer for the player as he chooses his class
	Unit* classChoice = unit;
	cout << "Please select a class." << endl;
	cout << "1. Warrior" << endl;
	cout << "2. Mage" << endl;
	cout << "3. Assassin" << endl;
	int choice;
	cin >> choice;

	//choice = warrior
	if (choice == 1)
	{
		cout << endl << "A strong choice!" << endl;
		cout << "These are your statistics." << endl;
		cout << "Name: " << name << endl;
		warriorStats(name, classChoice); //contains warrior stats
		displayInfo(classChoice); //prints stats

		unit = classChoice;

		return classChoice;
	}

	//choice = mage
	else if (choice == 2)
	{
		cout << endl << "A magical decision!" << endl;
		cout << "These are your statistics." << endl;
		cout << "Name: " << name << endl;
		mageStats(name, classChoice); //contains mage stats
		displayInfo(classChoice); //prints stats
		
		unit = classChoice;

		return classChoice;
	}

	//choice = assassin
	else if (choice == 3)
	{
		cout << endl << "A stealthy one!" << endl;
		cout << "These are your statistics." << endl;
		cout << "Name: " << name << endl;
		assassinStats(name, classChoice); //contains assassin stats
		displayInfo(classChoice); //prints stats
		
		unit = classChoice;

		return classChoice;
	}
	//aka playerClass in int main()
	return classChoice;
}

//contains the values and attributes of the warrior class
void warriorStats(string name, Unit* unit)
{
	unit->mName = name;
	unit->mClass = "Warrior";
	unit->mHp = 100;
	unit->mPow = 60;
	unit->mVit = 20;
	unit->mAgi = 40;
	unit->mDex = 20;
	unit->mKey = 1;
	unit->mMaxHp = 100; //used later for finding 30% of HP for recovery
}

//contains the values and attributes of the mage class
void mageStats(string name, Unit* unit)
{
	unit->mName = name;
	unit->mClass = "Mage";
	unit->mHp = 100;
	unit->mPow = 50;
	unit->mVit = 30;
	unit->mAgi = 30;
	unit->mDex = 25;
	unit->mKey = 2;
	unit->mMaxHp = 100; //used later for finding 30% of HP for recovery
}

//contains the values and attributes of the assassin class
void assassinStats(string name, Unit* unit)
{
	unit->mName = name;
	unit->mClass = "Assassin";
	unit->mHp = 100;
	unit->mPow = 40;
	unit->mVit = 20;
	unit->mAgi = 45;
	unit->mDex = 30;
	unit->mKey = 3;
	unit->mMaxHp = 100; //used later for finding 30% of HP for recovery
}

//prints the stats of each character class
void displayInfo(Unit* unit)
{
	cout << "Class: " << unit->mClass << endl;
	cout << "Health: " << unit->getHp() << endl;
	cout << "Power: " << unit->getPow() << endl;
	cout << "Vitality: " << unit->getVit() << endl;
	cout << "Agility: " << unit->getAgi() << endl;
	cout << "Dexterity: " << unit->getDex() << endl;
}

//determines which classes are stronger and weaker against the other
float classStrength(Unit* attacker, Unit* defender)
{
	//pointers for the attacker and defender
	Unit* attackerStrength = attacker;
	Unit* defenderStrength = defender;
	float bonusDamage{};

	// key 1 = Warrior
	// key 2 = Mage
	// key 3 = Assassin

	if (attackerStrength->mKey == 1)
	{
		if (defenderStrength->mKey == 3)
		{
			//attacker is stronger than the defender
			bonusDamage = 1.5;
		}

		else
		{
			//equal strength or attacker is weaker than defender
			bonusDamage = 1;
		}
	}

	if (attackerStrength->mKey == 2)
	{
		if (defenderStrength->mKey == 1)
		{
			//attacker is stronger than the defender
			bonusDamage = 1.5;
		}

		else
		{
			//equal strength or attacker is weaker than defender
			bonusDamage = 1;
		}
	}

	if (attackerStrength->mKey == 3)
	{
		if (defenderStrength->mKey == 2)
		{
			//attacker is stronger than the defender
			bonusDamage = 1.5;
		}

		else
		{
			//equal strength or attacker is weaker than defender
			bonusDamage = 1;
		}
	}
	//to be used for computing the attack damage
	return bonusDamage;
}

//creates a random enemy with a random class
Unit* enemySpawner(string name, Unit* unit)
{
	srand(time(NULL));
	Unit* randomEnemy = unit; //pointer for the enemy
	int r = rand() % 3 + 1; //random output between the 3 classes

	if (r == 1) //using mKey 
	{
		warriorStats(name, randomEnemy); //assigning warrior stats to enemy
		return randomEnemy;
	}

	else if (r == 2)
	{
		mageStats(name, randomEnemy); //assigning mage stats to enemy
		return randomEnemy;
	}

	else if (r == 3)
	{
		assassinStats(name, randomEnemy); //assigning assassin stats to enemy
		return randomEnemy;
	}
	//to be used later for playRound
	return randomEnemy;
}

//actual game
void playRound(Unit* playerClass, Unit* enemy, int round)
{
	Unit* player = new Unit; //pointer for player
	player = playerClass;

	cout << "Well, " << player->mName << ", let us go on your journey!" << endl;

	while (player->alive())
	{
		srand(time(NULL)); //for the randomizer in hitRate
		round++; //increases every time loop repeats, counting number of rounds
		Unit* newEnemy = new Unit; //pointer for enemy
		newEnemy = enemy;

		cout << "...Wandering..." << endl;
		cout << "...Wandering..." << endl;
		cout << "...Wandering..." << endl;
		cout << "You've encountered an enemy!" << endl;
		cout << endl << "These are its statistics:" << endl;

		newEnemy = enemySpawner("Monster", enemy); //creates new random enemy
		newEnemy->enemyStatBonus(newEnemy, round); //makes enemies stronger with each round
		displayInfo(newEnemy); //print enemy stats

		cout << endl << "Who will attack first?" << endl;
		
		system("pause"); //added throughout whole project to produce better pacing for the user

		if (player->getAgi() < newEnemy->getAgi()) //checks who attacks first based on agility
		{
			cout << endl << "The ENEMY has the first move!" << endl; //enemy attacks first (higher agility)

			system("pause");

			while (player->alive() && newEnemy->alive()) //ensures that attacks are only done if both are still alive
			{
				float enemyBonus = classStrength(newEnemy, player); //determining enemy damage bonus
				cout << endl << newEnemy->mName << " moves to attack!" << endl;
				int random1 = rand() % 80 + 20; //randomizer for the hitRate
				newEnemy->hitRate(newEnemy, player, enemyBonus, random1); //contains hitRate and damage by enemy

				if (newEnemy->alive() && player->getHp() <= 0) //conditions where player loses
				{
					cout << endl;
					lose(player, newEnemy, round); //shows game progress after losing
					break;
				}

				system("pause");
				cout << endl;

				float playerBonus = classStrength(player, newEnemy); //determining player damage bonus
				cout << endl << player->mName << " moves to attack!" << endl;
				int random2 = rand() % 80 + 20; //randomizer for the hitRate
				player->hitRate(player, newEnemy, playerBonus, random2); //contains hitRate and damage by player
				
				if (player->alive() && newEnemy->getHp() <= 0) //conditions where player wins
				{
					cout << endl;
					win(player, newEnemy); //player rewards for winning
					cout << endl;
					break;
				}
	
				system("pause");
				cout << endl;
			}
		}
		//if equal agility or player has a higher agility
		else if (player->getAgi() == newEnemy->getAgi() || player->getAgi() > newEnemy->getAgi())
		{
			cout << endl << "YOU have the first move!" << endl;

			system("pause");

			while (player->alive() && newEnemy->alive()) //ensures that attacks are only done if both are still alive
			{
				float playerBonus = classStrength(player, newEnemy); //determining player damage bonus
				cout << endl << player->mName << " moves to attack!" << endl;
				int random1 = rand() % 80 + 20; //randomizer for the hitRate
				player->hitRate(player, newEnemy, playerBonus, random1); //contains hitRate and damage by player

				if (player->alive() && newEnemy->getHp() <= 0) //conditions where player wins
				{
					cout << endl;
					win(player, newEnemy); //player rewards for winning
					cout << endl;
					break;
				}

				system("pause");
				cout << endl;

				float enemyBonus = classStrength(newEnemy, player); //determining enemy bonus damage
				cout << endl << newEnemy->mName << " moves to attack!" << endl;
				int random2 = rand() % 80 + 20; //randomizer for the hitRate
				newEnemy->hitRate(newEnemy, player, enemyBonus, random2); //contains hitRate and damage by enemy

				if (newEnemy->alive() && player->getHp() <= 0) //conditions where player loses
				{
					cout << endl;
					lose(player, newEnemy, round); //shows game progress after losing
					break;
				}
				
				system("pause");
				cout << endl;
			}
		}
		delete newEnemy; //deallocating objects no longer being used (as loop repeats)
	}
	delete player; //deallocating objects no longer being used (as game ends)
}

//contains rewards for the player winning
void win(Unit* player, Unit* enemy)
{
	cout << "You have won this round!" << endl;
	cout << "Your health has been replenished!" << endl;
	cout << "As the winner, your stats have improved!" << endl;
	cout << "Here are your current statistics:" << endl;

	player->statBonus(player, enemy); //player receives bonuses depending on the enemy defeated
	player->recoverHp(player); //player recovers 30 percent of his maximum health
	displayInfo(player); //prints current stats after bonus and recovery

	system("pause");
}

//shows the defeat and the game progress
void lose(Unit* player, Unit* enemy, int round)
{
	cout << enemy->mName << " is too strong!" << endl;
	cout << "You have died..." << endl;
	cout << "This is how far you've come:" << endl;
	cout << "You have reached STAGE: " << round << endl; //displays how many rounds the player reached before dying
	displayInfo(player); //prints player stats after dying
}