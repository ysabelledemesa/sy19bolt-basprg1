#include "Unit.h"

//getter for Name
string Unit::getName()
{
	return mName;
}

//getter for Health
int Unit::getHp()
{
	return mHp;
}

//getter for Power
int Unit::getPow()
{
	return mPow;
}

//getter for Vitality
int Unit::getVit()
{
	return mVit;
}

//getter for Agility
int Unit::getAgi()
{
	return mAgi;
}

//getter for Dexterity
int Unit::getDex()
{
	return mDex;
}

//ensures instance still has an HP above 0
bool Unit::alive()
{
	return mHp > 0;
}

//contains formula for damage created by the attacker against defender
int Unit::damage(Unit* attacker, Unit* defender, float bonusDamage)
{
	//damage = (POW of attacker - VIT of defender) * bonusDamage

	int damage = (attacker->mPow - defender->mVit) * bonusDamage; //bonusDamage determined in classStrength(source.cpp)

	defender->mHp -= damage;

	return damage;
}

//determines whether attack will hit or miss
void Unit::hitRate(Unit* attacker, Unit* defender, float bonusDamage, int random)
{
	//formula for hit rate of attacker against defender
	int hit = (static_cast<float>(attacker->mDex) / static_cast<float>(defender->mAgi)) * 100;
	if (hit > 80) hit = 80; //setting maximum hit rate at 80 percent
	if (hit < 20) hit = 20; //setting minimum hit rate at 20 percent

	cout << attacker->mName << " has a hit rate of " << hit << " percent!" << endl;

	if (hit >= random) //for this game, the set hit would happen if the hit rate is higher or equal to the random value
	{
		int damage = attacker->damage(attacker, defender, bonusDamage); //implementing damage using Unit::damage
		if (defender->mHp < 0) defender->mHp = 0; //ensuring that the HP does not reach a negative value
		cout << attacker->mName << " has dealt damage worth " << damage << endl;
		cout << defender->mName << "'s HP has gone down to " << defender->mHp << endl;
	}
	
	else if (hit < random) //and a miss would if the hit rate is below the random value 
	{
		cout << attacker->mName << "'s attack has missed!" << endl;
	}
}

//bonuses that the player receives upon winning, based on the defeated opponent
void Unit::statBonus(Unit* player, Unit* enemy)
{
	if (enemy->mKey == 1) //defeated an enemy warrior
	{
		player->mPow += 3;
		player->mVit += 3;
	}

	else if (enemy->mKey == 2) //defeated an enemy mage
	{
		player->mPow += 5;
	}

	else if (enemy->mKey == 3) //defeated an enemy assassin
	{
		player->mAgi += 3;
		player->mDex += 3;
	}
}

//player recovers 30 percent of his maximum HP after winning a round
void Unit::recoverHp(Unit* player)
{
	int recover = player->mMaxHp * 0.3;
	player->mHp += recover;
	if (player->mHp > 100) player->mHp == player->mMaxHp; //ensuring HP does not go above maximum
}

//enemy stats increase after every round
void Unit::enemyStatBonus(Unit* enemy, int round)
{
	for (int i = 0; i < 1; i++)
	{
		if (round >= 2)
		{
			enemy->mHp += 3;
			enemy->mPow += 3;
			enemy->mVit += 3;
			enemy->mAgi += 3;
			enemy->mDex += 3;
		}
	}
}
