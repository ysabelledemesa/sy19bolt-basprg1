#pragma once
#include <string>
#include <iostream>

using namespace std;

class Unit 
{
public:

	//character attributes
	string mName{}; //character name
	string mClass{}; //class
	int mHp{}; //health 
	int mPow{}; //power
	int mVit{}; //vitality
	int mAgi{}; //agility
	int mDex{}; //dexterity
	int mKey{}; //key number or ID number (to be used to later)
	int mMaxHp{}; //maximum health

	//getters
	string getName();
	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	bool alive(); //ensures that the instance still has an HP above 0
	//contains formula for damage created by the attacker against the defender
	int damage(Unit* attacker, Unit* defender, float bonusDamage);
	//determines whether attack will hit or miss
	void hitRate(Unit* attacker, Unit* defender, float bonusDamage, int random);
	//bonuses player receives upon winning, based on the defeated opponent
	void statBonus(Unit* player, Unit* enemy);
	//player recovers 30 percent of maximum HP after winning a round
	void recoverHp(Unit* player);
	//enemy stats increase after every round 
	void enemyStatBonus(Unit* enemy, int round);
};


